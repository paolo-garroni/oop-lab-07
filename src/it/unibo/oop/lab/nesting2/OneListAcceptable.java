package it.unibo.oop.lab.nesting2;

import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T> {

	private enum State {
		ACCEPTED, NOT_YET_ACCEPTED;
	}

	private final Map<T, State> map;

	public OneListAcceptable(final List<T> listToAccept) {
		List<T> list = new ArrayList<T>(Objects.requireNonNull(listToAccept));
		this.map = new HashMap<T, State>();
		for (T element : list) {
			this.map.put(element, State.NOT_YET_ACCEPTED);
		}
	}

	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if (OneListAcceptable.this.map.values().contains(newElement)) {
					State currentState = OneListAcceptable.this.map.get(newElement);
					if (currentState == State.NOT_YET_ACCEPTED) {
						currentState = State.ACCEPTED;
						return;
					}
				}
				throw new ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws EndNotAcceptedException {
				for (State elementState : OneListAcceptable.this.map.values()) {
					if (elementState == State.NOT_YET_ACCEPTED) {
						throw new EndNotAcceptedException();
					}
				}
			}

		};
	}

}
